# Learning Process
## 1. How to learn Faster with the Feynman Technique. 
### Question 1
#### What is the Feynman Technique? Paraphrase the video in your words.
This technique is named after the great Scientist Feynman Who Was an American theoretical physicit known for his work in the path integral formulation of quantum mechanics, the theory of quantum electrodynamics. He said that If we want to understand something well we need to explain it  to someone else. Explaining a concept works to improve our understanding of the concept . To have an indepth knowlegde of a topic, it is always good to explain the concept to someone else. 

* Feynman suggested four steps to make learning a simple process.

1. Take a peice of paper and write down the concept's name at the top.
2. Explain the concept using simple language and examples.
3. Identify the problem areas, then go back to the resources to review.
4. Identify the complex terms and break them down into simple words for understanding.

### Question 2
#### What are the different ways to implement this technique in your learning process?
* Different ways to implement this techninque in our learning process by 
1. Keep Learning New Things
2. Learn in Multiple Ways 
3. Self-Explanation
4. Teach What we are Learning 
5. Gain Practical Experience 
6. Elaborative Interrogation

## 2. Learning How to Learn TED talk by Barbara Oakley
### Question 3
#### Paraphrase the video in detail in your own words.
The TED Talker Barbara Oakley went in search of finding out effective ways of learning and had taken references even from the lives of Dali and Edison on how they kept themselves focused with their works. She researched neuroscience and cognitive psychology and reached out to talk to top experts in those fields. she found out the keys to learning effectively.

As we know, the brain is enormously complex. But we can simplify its operations into two fundamentally different modes. The first one is a focused mode and the second one is a diffuse mode. The focus mode is like, we turn our attention to something. The diffuse mode is like the relaxed mode. When we are focusing on something, trying to learn a new concept or, solving a problem we got stuck. We want to turn our attention away from that problem and allow the relax mode. So often we can get trapped in procrastination because the task we’re about to start is daunting in some way.

Hence she suggested the Pomodoro technique wherein we decide to have a mindset that we can learn for 25 minutes with full focus and take rest for the next 5 minutes. This 25 minutes of full focus is achieved by keeping ourselves away from any kind of distractions like mobile pings and this period is not to get all our work completed within 25 minutes but to do the learning mindfully. This way we are enhancing our ability for focused attention while learning.


### Question 4
#### What are some of the steps that you can take to improve your learning process?
* steps to improve our learning process 
1. Identify a task or tasks that we need to complete.
2. Set a timer for 25 minutes.
3. Work on a task with no distractions.
4. When the alarm sounds, take a 5-minute break.
5. Repeat the process 3 more times.
6. Take a longer 30-minute break and start again.
7. It is always good to test our level of learning by checking with mini tests.
## 3. Learn Anything in 20 hours
### Question 5
#### Your key takeaways from the video? Paraphrase your understanding.
* When we want to learn new things,Be clear about what to learn so that at the end we can get what we expected to achieve by learning the skill.
* With a little bit of practice we get better and better.
* If we want to learn any skill for ourselves we don't care so much about time,we just care about how good we are, whatever good happens to mean.
* The more time we spend on the skill, the better we become
* If we put 20 hours of focused deliberate pratice into thing we will be able to good at new skill.
* There is a better way to practice efficiently,make sure that we invest those 20 hours.
* so there are four important points to consider
1. Deconstruct the skill.
2. Learn Enough to self correct.
3. Remove barriers to practice.
4. Practice atleast 20 hours.
### Question 6
#### What are some of the steps that you can while approaching a new topic?
* Decide exactly what we want to be and then look into the skill and break it down into smaller pieces.
* The more we are able to break the skill, the more we are able to decide.
* we need to go through 3 to 5 resources about what it is and what we are trying to learn.
* The learning becomes better when we notice where we are making mistakes.
* Keep ourselves away from distractions while learning.







    

