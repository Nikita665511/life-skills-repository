
# SQL
- A report on SQL with code samples discussing basics,joins and aggregations.

## What is SQL ?
+ SQL stands for Structured Query Language.
+ SQL is a short-form of the structured query language, and it is pronounced as S-Q-L or sometimes as See-Quell.
+ It is also designed for stream processing in RDSMS.
+ We can easily create and manipulate the database, access and modify the table rows and columns, etc.
+ This query language became the standard of ANSI in the year of 1986 and ISO in the year of 1987.

## Why SQL ?
+ The basic use of SQL for data professionals and SQL users is to insert, update, and delete the data from the relational database.
+ SQL allows the data professionals and users to retrieve the data from the relational database management systems.
+ It also helps them to describe the structured data.
+ It also helps in creating the view, stored procedure, and functions in the relational database.
+ It allows you to define the data and modify that stored data in the relational database.
+ It also allows SQL users to set the permissions or constraints on table columns, views, and stored procedures

## What can SQL do ?
+ SQL can execute queries against a database
+ SQL can retrieve data from a database
+ SQL can insert records in a database
+ SQL can update records in a database
+ SQL can delete records from a database
+ SQL can create new databases
+ SQL can create new tables in a database
+ SQL can create stored procedures in a database
+ SQL can create views in a database
+ SQL can set permissions on tables, procedures, and views.

## SQL Commands:
 + The standard SQL commands to interact with relational databases are CREATE, SELECT, INSERT, UPDATE, DELETE and DROP
 
- These commands can be classified into the following groups based on their nature:

+ `CREATE` : This command helps in creating the new database, new table, table view, and other objects of the database.

+ `ALTER` : This command Modifies an existing database object, such as a table.


+ `DROP` : This command helps in deleting the entire table, table view, and other objects from the database.


## DML - Data Manipulation Language:

+ DDL or Data Definition Language actually consists of the SQL commands that can be used to define the database schema. It simply deals with descriptions of the database Schema
 and is used to create and modify the structure of database objects in the database. DDL is a set of SQL commands used to create, modify, and delete database structures but not data.


+ `Select`: This Retrieves certain records from one or more tables.

+ `Update`: This Command Modifies records.

+ `Delete`: This Command Deletes records.


## SQL JOINS:

+ As the name shows, JOIN means to combine something. In case of SQL, JOIN means "to combine two or more tables".

 The SQL JOIN clause takes records from two or more tables in a database and combines it together.

 ANSI standard SQL defines four types of JOIN :

1.inner join
2.left join
3.righ join
4.full join

### Visual Representation of SQL Joins:

+ INNER JOIN
+ LEFT JOIN
+ RIGHT JOIN
+ FULL JOIN


### Inner Join:
![image](https://blog.codinghorror.com/content/images/uploads/2007/10/6a0120a85dcdae970b012877702708970c-pi.png)

+The INNER JOIN keyword selects all rows from both the tables as long as the condition is satisfied.This keyword will create the result-set by combining all rows from both the tables where the condition satisfies i.e value of the common field will be the same.This is the simplest, most understood Join and is the most common. 
This query will show the names and age of students enrolled in different courses.  


```
SELECT StudentCourse.COURSE_ID, Student.NAME, Student.AGE FROM Student
INNER JOIN StudentCourse
ON Student.ROLL_NO = StudentCourse.ROLL_NO;

```

### Left Join:

![image](https://i.stack.imgur.com/VkAT5.png)

+ This join returns all the rows of the table on the left side of the join and matches rows for the table on the right side of the join. For the rows for which there is no matching row on the right side, the result-set will contain null.
 LEFT JOIN is also known as LEFT OUTER JOIN.

```
SELECT Student.NAME,StudentCourse.COURSE_ID 
FROM Student
LEFT JOIN StudentCourse 
ON StudentCourse.ROLL_NO = Student.ROLL_NO;

```

### Right Join:


 ![image](https://media.geeksforgeeks.org/wp-content/uploads/20220515095048/join.jpg)

 +  RIGHT JOIN is similar to LEFT JOIN. This join returns all the rows of the table on the right side of the join and matching rows for the table on the left side of the join.
 For the rows for which there is no matching row on the left side, the result-set will contain null. RIGHT JOIN is also known as RIGHT OUTER JOIN. 

```
SELECT Student.NAME,StudentCourse.COURSE_ID 
FROM Student
RIGHT JOIN StudentCourse 
ON StudentCourse.ROLL_NO = Student.ROLL_NO

```

 ### Full Join:

 ![image](https://i.stack.imgur.com/3Ll1h.png)

 + FULL JOIN creates the result-set by combining results of both LEFT JOIN and RIGHT JOIN. The result-set will contain all the rows from both tables.
   For the rows for which there is no matching, the result-set will contain NULL values.

```
SELECT Student.NAME,StudentCourse.COURSE_ID 
FROM Student
FULL JOIN StudentCourse 
ON StudentCourse.ROLL_NO = Student.ROLL_NO;

```


## AGGREGATIONS:

+ Aggregate functions in SQL Server are used to perform calculations on one or more values and return the result in a single value. In SQL Server, all aggregate functions are built-in functions that avoid NULL values except for COUNT(*).
  We mainly use these functions with the GROUP BY and HAVING clauses of the SELECT statements in the database query languages.

+ SQL Server provides various aggregate functions, and the most commonly used aggregate functions are shown in the below
  
* count()
* sum()
* Avg()
* Min()
* Max()
  
### Count() Function

+ This function returns the total number of rows, including NULL values in the given expression. It can also count all records based on a specified condition 
and returns zero if it does not find any matching records. It can work with both numeric and non-numeric data types.

- Example

+ The below example uses the COUNT() function and returns the total number of employees data stored in the employee table:

 `SELECT COUNT(*) AS total_employees FROM employee;` 

- Output :

 ![image]( https://static.javatpoint.com/sqlserver/images/sql-server-aggregate-functions2.png)

### Sum() Function

+ This function calculates the total summation of NON-NULL values in the given set. It returns NULL if the result set does not have any records. 
  The SUM function can only work with the numeric data type.

- Example

+ The below example uses the SUM function and calculates the total summed up salary of all employees stored in the employee table:
 
  `SELECT SUM(salary) AS total_salary FROM employee;`   

 - Output :
   
 ![image](https://static.javatpoint.com/sqlserver/images/sql-server-aggregate-functions3.png)

 ### Avg() Function
 
 + This function calculates the average of NON-NULL values specified in the column. The AVG function can only work with the numeric data type.

 - Example

 + The below example uses the AVG function and calculates the average salary of employees stored in the employee table:
 
  `SELECT AVG(salary) AS "Average Salary" FROM employee;`

 - Output :
  
  ![image](https://static.javatpoint.com/sqlserver/images/sql-server-aggregate-functions4.png)
 
 ### Min() Function
 
 + This function gives the minimum (lowest) value of the specified column. It also works with numeric data types only.
 
 - Example

 + The below example uses the MIN function and returns the lowest salary of an employee stored in the employee table:
 
 `SELECT MIN(salary) AS "Lowest Salary" FROM employee;` 

 - Output :

 ![image](https://static.javatpoint.com/sqlserver/images/sql-server-aggregate-functions5.png)

### Max() Function
 
+ This function gives the maximum (highest) value of the specified column. It also works with numeric data types only.

- Example

+ The below example uses the MAX function and returns the highest salary of employees stored in the employee table:

 `SELECT MAX(salary) AS "Highest Salary" FROM employee;`
 
 - Output :

 ![image](https://static.javatpoint.com/sqlserver/images/sql-server-aggregate-functions6.png)



### REFERENCES:

* W3Schools(https://www.w3schools.com/sql/sql_intro.asp#:~:text=What%20is%20SQL%3F,for%20Standardization%20(ISO)%20in%201987)
* javatpoint(https://www.javatpoint.com/sql-tutorial)
* Tutorialspoint(https://www.tutorialspoint.com/sql/sql-overview.htm)
* Geeks for geeks(https://www.geeksforgeeks.org/sql-tutorial/)
* Khanacademy.org(https://www.khanacademy.org/computing/computer-programming/sql)
