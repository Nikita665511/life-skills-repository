# Listening and Active Communication
## 1. Active Listening
### Question 1
#### What are the steps/strategies to do Active Listening?

1. Avoid getting distracted by your own thoughts.
2. Focus on the speaker and topic.
3. Don't interrupt while the other person is talking let them finish and then repond.
4. show that you're interested and keep the other person talking.
5. Take notes during important conversations.
6. Listen without judging, or jumping to conclusions.

## 2. Reflective Listening
### Question
### According to Fisher's model, what are the key points of Reflective Listening?
1. We need to listen more than we talk.
2. Allow speakers to completely state their thoughts or opinions without interrupting.
3. Try to remember the important facts or points made by others.
4. note down any details or points raised by others.
5. show genuine interest in the conversation of others.
6. Avoid making judgements.

## 3. Reflection
### Question 3
#### What are the obstacles in your listening process?

1. The external noises like the sound of equipment running, phones ringing, or other people having conversations 
2. Visual distractions like the scene outside a window or the goings-on just beyond the glass walls of a nearby office.
3. An uncomfortable temperature, poor or nonexistent seating, bad odors, or distance between the listener and speaker can be an issue.
4. Items like pocket change, pens, and jewelry are often fidgeted with while listening.
5. sometimes we can distracted by the other person’s personal appearance, mannerisms, voice, or gestures.
6.sometimes vehicle sounds can be a barrier.
### Question 4
#### What can you do to improve your listening?
1. Maintaining eye contact with the speaker
2. By Visualize what the speaker is saying.
3. Don't interrupt while the other person is speaking.
4. waiting for the speaker to pause to ask clarifying questions.
5. Be aware Of nonverbal cues.
6. Be Aware Of Nonverbal Signs, Such As Body Language And Tone Of Voice.
## 4. Types of Communication
### Question 5
#### When do you switch to Passive communication style in your day to day life?
1. I will switch to passive communication in family gatherings.
2. In  some meetings if i'm not interested.
3. When someone asks me help whether i'm comfortable or not i never say No.
### Question 6
#### When do you switch into Aggressive communication styles in your day to day life?
1. when someone talk unnecessarily.
2. When someone speaks loudly.
3. When I was scolded for no reason.
### Question 7
#### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
1. I will switch to passive aggressive when someone speaks with me continously.
2. when someone cracks jokes on me.
### Question 8
#### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? 
1. I will learn how to say no, when i am not okay with that thing.
2. understanding the other point of view.
3. Leaving negative emotions.
4. Always thinks in positive way.
5. stand for ourself.












