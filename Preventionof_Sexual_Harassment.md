# Prevention of Sexual Harassment

### What kinds of behaviour cause sexual harassment?

sexual harassment is any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.
* There are generally three forms of sexual harassment .
1. verbal
2. visual
3. physical 

 * **Verbal** :  verbal harassment includes 
 1. comments about clothing ,person's body, sexual or gender based jokes or remarks.
 2. Requesting sexual favors or repeatedly asking a person out sexual innuendos threats, spreading rumours about a person's personal or sexual life or using foul and obscene language.
 * **Visual** : visual harassment includes 
 1. obscene posters
 2. Drawings
 3. pictures
 4. screensavers
 5. cartoons emails
 6. texts of a sexual nature.
 * **Physical** : physical harassment includes
 1. sexual  assualt impeding or blocking movement.
 2. Inappropriate touching such as kissing,hugging,patting or stroking or rubbing.
 3. sexual gesturing.
 4. leering or staring.
 * There are two categories of sexual harassment 
 * **QUID-PRO-QUO**
    
     It means this for that.
     This happens when an employer or supervisor uses job rewards or punishments to coerce an employee into a sexual relationship or sexual act this includes offering the employee raises or promotions or threatining, firing or other penalties for not complying with request.
* **Hostile Work Environment**
    
    It occurs when employee behaviour interfers with the work performance of another or creates an intimating or offensive workplace this usually happens when someone makes repeated repeated sexual comments and makes another employee  feel so uncomfortable that their work performance suffers or they decline professional opportunities.

    ### What would you do in case you face or witness any incident or repeated incidents of such behaviour?


    1. The date, time and location of the harassment, what happened, what was said and who witnessed the behavior.I will 
    Keep copies or take screenshots of any relevant emails, texts, photos or social posts. 
    2. I will tell  to a trusted friend, family member or co-worker what happened and write down the details of those conversations. Not only can they provide support. but they may also be able to provide corroborating statements. 
    3. Keep records related to my productivity and job performance and, if possible, review my performance report or personnel file. This is so I have evidence should my performance ever be disputed.
    4. Store all documentation outside my office or my working computer and make sure it’s backed up in a safe place. 
    5. I would follow the procedures and complaint with ICC(Internal Complain Committee) and keep a record of it.  
    6. In extreme cases, I will file a police complaint or contact a Lawyer. 

    





    



